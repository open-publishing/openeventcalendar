const data = require('./views/_data/config.json')
const theme = data.theme;

module.exports = {
  scripts : {
    "mount:eleventy": "mount temp --to /",
    "mount:js": "mount static/js --to /js",
    "mount:css": "mount temp/css --to /css",
    
    
    "mount:data": "mount static/data --to /data",
    "mount:fonts": `mount static/css/themes/${theme}/fonts/ --to /css/fonts/`,
    "run:eleventy": "eleventy",
    "run:eleventy::watch": "$1 --serve",
    "run:css": `postcss static/css/themes/${theme}/source.css -o temp/css/main.css`,
    "run:css::watch": "$1 --watch"
  },
  installOptions: {
    polyfillNode: true,
    rollup: {
      plugins: [require("rollup-plugin-node-polyfills")()],
    },
  },
  devOptions: {
    port: 3000,
    open: "none",
    out: "public",
    hmr: true,
    hmrdelay: 300,
    bundle: false
  },
};

