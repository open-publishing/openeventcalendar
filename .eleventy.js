var momentz = require("moment-timezone");
var config = require("./views/_data/config.json");

// markdown parser
let markdownIt = require("markdown-it");
let implicitFigures = require("markdown-it-implicit-figures");
let blockEmbedPlugin = require("markdown-it-block-embed");
let html5Embed = require("markdown-it-html5-embed");
let frame = require("markdown-it-iframe");

module.exports = function (eleventyConfig) {
  // markdown

  let markdownLib = markdownIt({ html: true })
    .use(implicitFigures, {
      dataType: false, // <figure data-type="image">, default: false
      figcaption: false, // <figcaption>alternative text</figcaption>, default: false
      tabindex: true, // <figure tabindex="1+n">..., default: false
      link: true, // <a href="img.png"><img src="img.png"></a>, default: false
    })
    .use(frame)
    .use(blockEmbedPlugin, {
      containerClassName: "video-embed",
      outputPlayerSize: false,
    })
    .use(html5Embed, {
      html5embed: {
        useImageSyntax: true, // Enables video/audio embed with ![]() syntax (default)
        useLinkSyntax: true, // Enables video/audio embed with []() syntax
      },
    });

  eleventyConfig.setLibrary("md", markdownLib);

  eleventyConfig.addFilter("markdownify", (value) => markdownLib.render(value));

  // multi filter
  eleventyConfig.addFilter("where", function (array, key, value) {
    return array.filter((item) => {
      const keys = key.split(".");
      const reducedKey = keys.reduce((object, key) => {
        return object[key];
      }, item);

      return reducedKey === value ? item : false;
    });
  });

  eleventyConfig.addFilter("toUTC", function(date, timezone ) {
    return momentz(`${date}`,timezone).utc().format();
  })


  // format date using moment
  eleventyConfig.addFilter("formatDate", function (
    value,
    targetTimeZone = "UTC",
    format
  ) {
    return momentz(value).tz(targetTimeZone).format(format);
  });

  // find offset between dates using moment: this was used to show empty dates between events, but it needs to be rewritten.
  eleventyConfig.addFilter("momentInterval", function (lastDate, firstDate) {
    const start = momentz(firstDate);
    const end = momentz(lastDate);
    let offset = end.diff(start, "days");
    let emptyDays = ``;
    let dayPassed = 0;
    while (dayPassed < offset) {
      emptyDays += `<ul class="day empty ${momentz(firstDate)
        .add(dayPassed + 1, "days")
        .format("dddd")}" id="day-${momentz(firstDate)
        .add(dayPassed + 1, "days")
        .format("DD")}"><h2><span class="day-letter">${momentz(firstDate)
        .add(dayPassed + 1, "days")
        .format("dddd")}</span> <span class="number">${momentz(firstDate)
        .add(dayPassed + 1, "days")
        .format("DD")}</span></h2></ul>`;
      dayPassed = dayPassed + 1;
    }
    return emptyDays;
  });

  // implementation for the split
  eleventyConfig.addFilter("splitter", function (value, sep = ",") {
    if (value.includes(sep)) {
      return value.split(sep);
    }
  });


  eleventyConfig.setTemplateFormats(["njk", "md"]);
  // trying to put things through snowpack and not eleventy
  eleventyConfig.addPassthroughCopy({ "static/data": "/data" });
  eleventyConfig.addPassthroughCopy({ "static/images": "/images" });
  
  // ↓ taking care of in snowpack
  // eleventyConfig.addPassthroughCopy({ "static/js": "/js" });
  
  // ↓ taking care of in snowpack
  // eleventyConfig.addPassthroughCopy({ "static/fonts": "/fonts" });
  // eleventyConfig.addPassthroughCopy({ "static/css": "/css" });




  eleventyConfig.addCollection('event', function(collection) {
    return collection.getFilteredByGlob('views/content/eventList/*.md').sort(function(a, b) {
      return b.fullTime - a.fullTime;
    })
  })

  return {
    dir: {
      input: "views",
      output: "temp",
    },
    passthroughFileCopy: true,
  };
};
