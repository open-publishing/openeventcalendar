// if no js show everything, else: 

document.querySelectorAll(".participant").forEach(el=> {el. classList.remove("active")
});
document.querySelector(".button").classList.remove("hide");

// show people
document.querySelectorAll(".participant").forEach(
    el => {
        el.addEventListener("click", () => {el.classList.toggle("active")});
    }
)




document.querySelector("input#search").addEventListener("keyup", function () {
    searchAnEvent(this);
});


function searchAnEvent(input) {

    // const input = document.querySelector('#search');
    let filter = input.value.toUpperCase();
    let eventList = document.querySelector("#participants").querySelectorAll('.participant');

    // Loop through all list items, and hide those who don't match the search query
    eventList.forEach(event => {
        let txtValue = event.textContent.toUpperCase() || event.innerText.toUpperCase();
        if (txtValue.indexOf(filter) > -1) {
            event.style.display = "";
        } else {
            event.style.display = "none";
        }
    })
}

