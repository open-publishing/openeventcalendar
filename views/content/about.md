---
layout: singlepage.njk
title: "Open Publishing Fest"
class: "about"
tags: 
  - about
permalink: /about.html
---

Open Publishing Fest is a decentralized public event that brings together communities supporting open source software, open content, and open publishing models.

Held over two weeks in May, Open Publishing Fest will feature discussions, demos, and performances that showcase our paths toward a more open world.

How will we shape the knowledge that will shape the future?