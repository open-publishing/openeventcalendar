---
title: Les métiers du texte
presenter: Martine Passelaigue
structure: Collectif La fabrique des mots, France
date: 2020-11-2T12:30:00Z
fullTime: 2020-11-2T12:30:00Z
timezone: Europe/Geneve
permalink: false
id: 6
tags: 
  -  event
---

Quel que soit le support ou le format, un texte requiert impérativement une vigilance particulière avant d’être publié. Par égard pour le lectorat, mais aussi pour la langue même – en ce qu’elle est matière première de la communication – et les idées qu’elle véhicule, les mots doivent faire l’objet d’une approche sensible. Plus largement, la diffusion plurilingue est désormais une évidence et soulève un ensemble de questions qui appellent autant de réponses que de cas. Traduire quoi, comment, pour qui ? La traduction est-elle bonne ? Qui va en juger ? Jusqu’où aller dans le passage d’une langue à l’autre ? Traduire, relire et corriger consistent à trouver la justesse de la formulation, à respecter les singularités et la tonalité de chaque langue, à concevoir le plaisir du texte.