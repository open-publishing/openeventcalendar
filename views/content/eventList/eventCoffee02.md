---
title: Accueil & café
presenter: Everyone
structure: HEAD – Genève
date: 2020-11-3T7:00:00Z
fullTime: 2020-11-3T7:00:00Z
timezone: Europe/Geneve
permalink: false
id: 14
tags: 
  -  event
---

Bonjour et bienvenue !

Malgré le plaisir que nous aurions pu partager autour du café, nous vous proposons de nous retrouver ici: [lien vers la vidéo](#).
