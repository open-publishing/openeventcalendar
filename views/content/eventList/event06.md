---
title: La mutation de la distribution du livre d’art
presenter: Emerick Charpentier
structure: Interart, Paris
date: 2020-11-2T13:30:00Z
fullTime: 2020-11-2T13:30:00Z
timezone: Europe/Geneve
permalink: false
id: 7
tags: 
  -  event
---

Dans un monde de l’édition en mutations, le domaine du livre d’art est particulièrement impacté. Par les coûts de production élevés des ouvrages et la faiblesse des tirages vendus, c’est le secteur de l’édition qui est le moins adapté au fonctionnement capitalistique de la distribution. La diffusion et la distribution dans ce domaine ne peuvent être viable qu’en ramenant ce travail à un niveau artisanal de proximité afin de pouvoir toucher le public cible.