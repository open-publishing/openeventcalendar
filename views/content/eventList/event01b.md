---
title: Introduction et présentation de HEAD – Publishing
presenter: 
structure: HEAD – Genève
date: 2020-11-2T7:30:00Z
fullTime: 2020-11-2T7:30:00Z
timezone: Europe/Geneve
permalink: false
id: 1
tags: event
---

Publier comme intervention spécifique et critique a pris l’ampleur comme pratique en art & design grâce aux outils contemporains de collaboration et production. Concevoir les flux de publications devient ainsi un des enjeux majeurs dans les projets de publication contextualisés, car cela permet de créer des conditions de travail pour rendre le projet durable et d’exposer les valeurs de l’équipe derrière la publication. Cette intervention présentera ainsi une sélection de projets comprenant une réflexion sur les flux de publication multisupports.
