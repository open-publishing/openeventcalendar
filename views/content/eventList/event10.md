---
title: Systèmes alternatifs de publication
presenter: Sarah Garcin 
structure: collectif L’Atelier des chercheurs, Paris
date: 2020-11-3T11:30:00Z
fullTime: 2020-11-3T11:30:00Z
timezone: Europe/Geneve
permalink: false
id: 11
tags: 
  -  event
---

Le logiciel libre, les standards ouverts et la diversité des outils open source incitent aujourd’hui le designer à se questionner sur ses choix technologiques et sur son identité. Auteur sur plusieurs plans, il est invité à ne plus subir les outils mais à les apprendre, les modifier ou les créer. La programmation devient alors un outil de design permettant de réinventer sans cesse le processus de création éditoriale en questionnant les formats de publication, mais aussi de fabriquer des outils répondant aux besoins d’un contexte précis. Le designer graphique n’est plus lié à la forme historique de l’imprimé. Il est désormais auteur, développeur, contributeur et hacker. Il se donne le droit de revendiquer son processus et d'en accepter la forme résultante. Il collabore, transmet, transforme et interprète.

