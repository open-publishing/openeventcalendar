---
title: Les différentes étapes de l’édition
presenter: Léa Gallon
structure: ancienne éditrice et designer graphique aux éditions Entremonde, Paris & Genève
date: 2020-11-2T9:30:00Z
timezone: Europe/Geneve
permalink: false
id: 4
tags: event
---

L’éditrice ou l’éditeur est le chef d’orchestre de la réalisation d’un livre. Comme l’a justement souligné Eric Hazan (fondateur de La Fabrique), être éditeur c’est « une aventure semée d’hésitations et de cahots, de rencontres précieuses et d’hostilités sournoises, mais toujours guidée par le souci collectif de subvertir l’ordre établi ». L’éditeur est le point névralgique vers lequel convergent tous les métiers de la conception à la production du livre, de l’auteur à la distribution, en passant par l’impression, la traduction, le design graphique, etc.