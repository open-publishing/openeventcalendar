---
title: Accueil & café day 1
presenter:
structure: HEAD – Genève
date: 2020-11-2T7:00:00Z
fullTime: 2020-11-2T7:00:00Z
timezone: Europe/Geneve
permalink: false
id: 0
tags: 
  -  event
---

Bonjour et bienvenue !

Malgré le plaisir que nous aurions pu partager autour du café, nous vous proposons de nous retrouver ici: [lien vers la vidéo](#).
