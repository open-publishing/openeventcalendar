---
title: Programmer l’imprimé — quatre catalogues d’exposition
presenter: Élise Gay & Kévin Donnot 
structure: atelier E+K, Paris
date: 2020-11-3T12:30:00Z
fullTime: 2020-11-3T12:30:00Z
timezone: Europe/Geneve
permalink: false
id: 12
tags: 
  -  event
---

Cette intervention détaille les processus mis en œuvre pour la conception des catalogues de la série de quatre expositions du cycle « Mutations/Créations » présentées au Centre Pompidou (Paris). Les ouvrages, conçus comme des projets-manifestes en écho à la thématique de chaque exposition, explorent les intrications possibles entre techniques numériques, design éditorial et mise en forme graphique par la création de programmes dédiés : typographie générative (*Imprimer le monde*, 2017), générateur hypertextuel (*Coder le monde*, 2018), automates cellulaires (*La fabrique du vivant*, 2019) ou machine learning appliqué à l’écrit (*Neurones, les intelligences simulées*, 2020).


