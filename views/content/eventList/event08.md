---
title: Low-Tech Magazine, Barcelone
presenter: Kris de Decker
structure: Low-Tech Magazine, Barcelone
date: 2020-11-3T8:30:00Z
fullTime: 2020-11-3T8:30:00Z
timezone: Europe/Geneve
permalink: false
id: 9
tags: 
  -  event
---

*Low-tech Magazine* remet en question la croyance dans le progrès technologique et s’appuie sur les connaissances et technologies du passé pour concevoir une société durable. Pour la refonte du site Web de la revue, nous avons conçu une architecture technique low-tech répondant à nos besoins et en adéquation à nos principes. Pour réduire la consommation d’énergie, nous avons opté pour une conception « *back-to-basic* », utilisant des pages Web statiques au lieu d’un habituel système de gestion de contenu (CMS) à base de données. Parce qu'il consomme peu d'énergie, le site Web de la revue peut fonctionner sur un mini-serveur alimenté par un panneau solaire situé sur un balcon. Pour limiter l’énergie intrinsèque du panneau solaire et de la batterie, le site se met hors ligne en cas de mauvais temps.