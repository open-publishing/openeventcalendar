---
title: Tangibles
presenter: Xavier Erni & Thuy-An Hoang
structure: Studio NeoNeo, enseignants à la HEAD – Genève
date: 2020-11-2T11:30:00Z
fullTime: 2020-11-2T11:30:00Z
timezone: Europe/Geneve
permalink: false
id: 5
tags: 
  -  event
---

Malgré une évolution évidente de l’édition vers les supports numériques, nous sommes encore particulièrement attachés au support imprimé. Le choix des matériaux, des techniques d’impression, de reliure et de façonnage sont pour nous des étapes clé dans l’aboutissement du processus créatif. À travers cela, nous sommes amenés à travailler en étroite collaboration avec divers corps de métier. C’est sans doute dans cette phase que notre travail prend vie et s’ancre dans la réalité, avec ses qualités et ses imperfections. Nous présenterons une sélection d’ouvrages réalisés ces dernières années dans le domaine de l’art et du design. A travers des exemples concrets, nous évoquerons le processus de conception, l’exécution, la production et la diffusion d’un objet éditorial.