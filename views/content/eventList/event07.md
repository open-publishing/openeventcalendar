---
title: Graphisme en France, la pédagogie par l’exemple
presenter: Véronique Marrier
structure: Cnap, Paris
date: 2020-11-3T7:30:00Z
fullTime: 2020-11-3T7:30:00Z
timezone: Europe/Geneve
permalink: false
id: 8
tags: 
  -  event
--- 

Le *Centre national des arts plastiques* publie depuis 1994, la revue annuelle Graphisme en France. Publiée à 10 000 exemplaires, elle s’adresse aux professionnels, aux étudiant·e·s et à un plus large public. Depuis 2014, elle est également disponible en anglais dans une version numérique. La revue aborde chaque année un thème différent (signalétique, numérique, recherche, logo-identité visuelle, typographie, etc.), pour lequel des textes sont commandés à de jeunes chercheurs, universitaires, historiens ou designers. Conçu chaque année par un jeune professionnel, l’expérimentation graphique y est encouragée, et tout est systématiquement redéfini : format, papier, caractère typographique et mise en page en fonction du thème, favorisant par là un exemple de ce que le design graphique peut apporter à l’édition.

 s