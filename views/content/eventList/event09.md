---
title: Design in translation
presenter: Julie Blanc & Julien Taquet 
structure: EUR ArTeC / Université Paris 8 / EnsadLab & Cabbage Tree Labs / Coko
date: 2020-11-3T9:30:00Z
fullTime: 2020-11-3T9:30:00Z
timezone: Europe/Geneve
permalink: false
id: 10
tags: 
  -  event
---

Les transformations des productions éditoriales liées à l'apparition du Web ne semblent pas encore avoir atteint les processus de fabrication des revues et des livres qui restent confinés à des outils en  silo entravant la collaboration entre les différents acteur·ices. Pour concevoir de nouveaux outils et *workflows* mieux adaptés à notre époque, les technologies du Web nous semblent être le point convergeant de tous les possibles. Grâce au design *responsive*, un même contenu HTML peut prendre toutes les formes possibles afin de produire des expériences de lecture multiples. L’imprimé n'est pas en reste, puisque nous avons conçu une solution libre et *open source* permettant la production de contenu imprimé à partir de HTML (paged.js). Par cette communication nous ferons un retour d’expérience sur ces nouvelles formes de publications numériques et imprimées, où standards et interopérabilité sont les maîtres mots.