---
title: Center for Future Publishing
presenter: Demian Conrad 
structure: Center for Future Publishing, Genève
date: 2020-11-3T13:30:00Z
fullTime: 2020-11-3T13:30:00Z
timezone: Europe/Geneve
permalink: false 
id: 13
tags: 
  -  event
---

Le métier de designer graphique subit de nombreux changements de définition et d’identité. Les médias numériques et les réseaux façonnent de nouveaux rapports à l’information. À l'époque de Gutenberg, les gens utilisaient le plomb et les mains ; à l'époque de Steve Jobs, la souris et le clavier. Aujourd'hui, nous sommes confrontés à de nouveaux paradigmes. Dans un avenir proche, les concepteurs utiliseront l'intelligence artificielle, les systèmes de conception générative, ou même les interactions entre le corps et la voix. Ces enjeux seront traités au travers de la présentation du Center for Future Publishing, un espace dédié à l’exploration et à l’expérimentation éditoriale en art et design.

