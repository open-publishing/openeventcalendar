---
title: Macula — 10 ans d’aventures éditoriales
presenter: Véronique Yersin & Joanna Schaffter
structure: Éditions Macula
date: 2020-11-2T8:30:00Z
timezone: Europe/Geneve
permalink: false
id: 3
tags: event
---

En 2010, une nouvelle équipe reprend les éditions Macula créées en 1980. Une ligne graphique est créée par le bureau Schaffter Sahli, avec qui la cinquantaine de livres publiés depuis vont être imaginés. La relation particulière qui lie un éditeur et un designer graphique sera présentée via deux cas concrets (les *Écrits complets* d’André Bazin et la revue *Transbordeur. Photographie, histoire, société*) permettant de considérer la dimension du travail d’équipe. L’intervention se terminera sur l’importance des directrices et directeurs de collections, qui alimentent grâce à leurs domaines de connaissances le catalogue de façon stimulante.  

