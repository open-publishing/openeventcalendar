---
layout: singlePageCal.njk
title: "Open Publishing Fest: the events" 
class: "calendar"
permalink: /index.html
timezoneLabel: Change the timezone
---

The calendar is updated constantly: dates and times for events may change and new events are added regularly.
Don’t forget that you can still [propose an event] and don’t forget to add your name to the people page! 