---
title: Léa Gallon
contact: lea.gallonbernard@gmail.com
tags: 
  - presenter
---

Léa Gallon est éditrice et designer graphique. Elle a terminé ses études en design graphique à la HEAD – Genève en 2016 et est diplômée du master CCC (programme de recherche d’Études critiques et pratiques curatoriales).  En 2016, avec trois camarades, elle relance les activités de la maison d'édition Entremonde, où elle est éditrice et graphiste. Elle a notamment travaillé sur la traduction d’ouvrages tels que *Caliban et la sorcière* de Silvia Federici, *Tout ce qui est solide se volatilise* de Marshall Berman, *Systèmes de grille* de Josef Muller-Brockmann, Le *Réalisme capitaliste* et *Ghost of my life* de Mark Fisher.