---
title: Sarah Garcin
contact: contact@sarahgarcin.com
tags: 
  - presenter
---

Sarah Garcin est designer graphique et développeuse. Elle s’intéresse aux pratiques collaboratives d’écriture et aux interfaces hommes-machines. Elle travaille au sein du collectif l’Atelier des Chercheurs (Paris) qui développe des outils tangibles et numériques de partage de connaissance et de documentation dans des contextes d’apprentissage. Elle mène au sein du collectif g-u-i une réflexion sur les nouvelles pratiques lié à l’édition et expérimente autour de systèmes alternatifs de publication. Elle a cofondé avec Raphaël Bastide le collectif PrePostPrint. Elle s’implique dans la communauté du logiciel libre et *open source* par l’utilisation et la distribution d’outils spécifiques qu’elle fabrique sous licence libre. Elle enseigne le design graphique à l’ENSAD Paris et partage ses pratiques lors d’ateliers en milieu scolaire et universitaire. Diplômée de l’École Nationale des Arts Décoratifs (DNSEP) et de l’EESAB Rennes (DNAP), elle vit et travaille à Paris.
