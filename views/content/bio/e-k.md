---
title: E + K
contact: 
  - elise@e-k.fr 
  - kevin@e-k.fr
tags: 
  - presenter
---

Élise Gay et Kévin Donnot sont associés au sein d’un atelier de design graphique (E+K) spécialisé dans les projets éditoriaux sur papier ou à l’écran. Ils sont par ailleurs cofondateurs de Back Office, une revue qui interroge les relations entre design graphique et pratiques numériques. Kévin Donnot enseigne également le design graphique à l’ÉESAB – site de Rennes (France).