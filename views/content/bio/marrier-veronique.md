---
title: Véronique Marrier
contact: veronique.marrier@culture.gouv.fr
tags: 
  - presenter
---

Véronique Marrier est responsable du service design graphique au Centre national des arts plastiques (Cnap, Paris).


