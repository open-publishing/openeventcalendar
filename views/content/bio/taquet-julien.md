---
title: Julien Taquet
contact: julien@lesvoisinsdustudio.ch
tags: 
  - presenter
---

Julien Taquet est designer. Il conçoit des outils de mise en pages libres et ouverts pour permettre au plus grand nombre de reprendre la main sur les outils de diffusion des contenus. Au sein du Cabbage Tree Labs, il s’occupe de Paged.js, et participe à la conception de plateformes de publication et d’outils de publications libres et *open source*, expérimentant autour des standards et du #CSSPrint