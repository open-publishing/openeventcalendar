---
title: Juan Gomez
contact: juan@juangomez.co
tags: 
  - presenter
---

Juan Gomez est designer-chercheur. Diplômé du Master Media Design de la HEAD – Genève  en 2019, il coordonne les contenus du Center for Future Publishing (Genève). Il réalise des projets d’édition multisupport ou hybrides, installations qui questionnent les outils contemporains du design graphique, et qui invitent les designers, développeurs, théoriciens et artistes à interroger l’impact des nouvelles technologies dans leurs pratiques. Auparavant, il a travaillé pour Integral Ruedi Baur (Paris), PublishingLab (Amsterdam), Atelier Malte Martin (Paris).

