---
title: Véronique Marrier
contact: anthony.masure@hesge.ch
tags: 
  - presenter
---

Anthony Masure est responsable de la recherche à la HEAD – Genève (IRAD, Institut de Recherche en Art & Design). Agrégé d’arts appliqués et ancien élève du département design de l’ENS Paris-Saclay, il est membre associé du laboratoire LLA-CRÉATIS de l’université Toulouse – Jean Jaurès. Ses recherches portent sur les implications sociales, politiques et esthétiques des technologies numériques. Il a cofondé les revues de recherche *Back Office* et *Réel-Virtuel*. Il est l’auteur de l’essai *Design et humanités numériques* (éd. B42, 2017).