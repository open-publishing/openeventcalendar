---
title: Joanna Schaffter
contact: joanna@schafftersahli.com
tags: 
  - presenter
---

Joanna Schaffter a étudié le design graphiques aux Arts Décoratifs de Genève. Après avoir travaillé pour Flavia Cocchi à Lausanne et Paula Scher au sein de Pentagram à New York, elle fonde en 2005 le bureau Schaffter Sahli avec Vincent Sahli. Le studio se spécialise dans la création d’identités visuelles comme pour la Head – Genève, ou encore la Villa Bernasconi à Genève. Schaffter Sahli étend ses compétences à la conception de livres. Depuis 2010 ils sont en charge de la conception ainsi que la réalisation de l’ensemble des ouvrage publiés par les éditions Macula.
