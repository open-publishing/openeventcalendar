---
title: NeoNeo
contact: 
  - xavier.erni@hesge.ch 
  - contact@neoneo.ch
tags: 
  - presenter
---

Xavier Erni et Thuy-An Hoang ont fondé en 2010 Neo Neo, un atelier de design graphique basé à Genève. Leurs domaines d’expertise comprennent le design éditorial, les identités visuelles, la conception de supports de communication et le dessin de caractères typographique. Ils publient également la revue *Poster Tribune* et dirigent Print Program, un espace d’exposition consacré aux objets graphiques imprimés. Ils enseignent à la HEAD – Genève dans le département de communication visuelle.

