---
title: Demian Conrad
contact: demian.conrad@hesge.ch
tags: 
  - presenter
---

Demian Conrad est designer graphique et chercheur. Il dirige depuis 2007 Automatico Studio, un studio de design graphique qui travaille au-delà des frontières des différents médias. Il travaille avec les nouvelles technologies, de la typographie interactive dans la conception d’expositions aux nouvelles méthodes de conception de livres générés par des programmes. En collaboration avec la HEAD – Genève, il a cofondé le Center for Future Publishing. Il enseigne le design éditorial à HEAD – Genève dans le département de communication visuelle.