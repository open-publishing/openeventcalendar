---
title: Julie Blanc
Contact : contact@julie-blanc.fr
tags: 
  - presenter
---

**Julie Blanc** est doctorante en design graphique et ergonomie (EUR ArTeC / Université Paris 8 – EA349 / EnsadLab). Elle contribue au développement de Paged.js et travaille à la composition et la mise en forme de différents projets éditoriaux multi-supports.
