---
title: Emerick Charpentier
Contact : emerick@interart.fr
tags: 
  - presenter
---

Emerick Charpentier, après dix années dans le tourisme et la banque d’affaires à Luxembourg, a repris des études en Métiers du Livre pour devenir libraire à l’Institut du Monde Arabe puis au Jeu de Paume. Recruté comme représentant par Interart, diffuseur et distributeur de livres d’art étranger en France en 2007, il y est dorénavant directeur commercial.