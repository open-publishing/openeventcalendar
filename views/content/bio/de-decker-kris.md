---
title: Kris De Decker
contact: kris@lowtechmagazine.com
tags: 
  - presenter
---

Kris De Decker est le fondateur de *Low-tech Magazine* (2007). Il travaille également sur des projets artistiques (notamment la Human Power Plant) et écrit pour des centres de recherche au Royaume-Uni (Demand Centre, CREDS). Il est né en Belgique et vit en Espagne.

