---
title: Julie Enckell Julliard
contact: julie.enckell-julliard@hesge.ch
tags: 
  - presenter
---

Julie Enckell Julliard est une curatrice et historienne de l’art suisse. Entre 2013 et 2017, elle a dirigé le Musée Jenisch Vevey, après y avoir occupé le poste de Conservatrice Art moderne et contemporain (2007–2013). Elle a conduit une cinquantaine de projets d’exposition et de publications, avec des artistes tels que Pierrette Bloch, Alain Huck, Ulla von Brandenburg, Thomas Hirschhorn, Francis Alÿs, Denis Savary etc. Docteure en histoire de l’art, elle est membre de la Commission fédérale d’art et du comité de la Société d’arts graphiques suisse (SGG). Depuis 2018, elle est responsable du développement culturel de la HEAD – Genève.
