---
title: Véronique Yersin
contact: vyersin@gmail.com
tags: 
  - presenter
---

Véronique Yersin est historienne de l’art. Elle travaille entre 2000 et 2010 au Cabinet des estampes du musée d’art et d’histoire de la Ville de Genève et auprès du conservateur Rainer Michael Mason. Pendant deux ans, elle codirige avec Madeleine Amsler l’espace d’art contemporain Forde à Genève (2008–2010). Elle prend la direction des Éditions Macula en 2010.