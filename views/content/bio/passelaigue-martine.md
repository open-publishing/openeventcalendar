---
title: Martine Passelaigue
contact: m.passelaigue@orange.fr
tags: 
  - presenter
---

Martine Passelaigue est titulaire d’un DEA de Linguistique/Études germaniques (Munich et Bordeaux). Traductrice diplômée et coordinatrice éditoriale dans le domaine de l’art, elle travaille depuis plus de trente ans avec de grands musées et éditeurs allemands, français et suisses (DTC, etc.).
